using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 velocity = Vector3.one;

    private void FixedUpdate()
    {
        transform.Rotate(velocity * Time.deltaTime);
    }
}