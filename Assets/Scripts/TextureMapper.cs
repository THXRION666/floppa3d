using UnityEngine;

using System.Collections.Generic;

public class TextureMapper : MonoBehaviour
{
    private float[,] bounds = new float[24, 2]
    {
        {0, 0}, {0.333f, 0}, {0, 0.333f}, {0.333f, 0.333f}, // front
        {0.334f, 0.333f}, {0.666f, 0.333f}, // top
        {1, 0}, {0.666f, 0}, // back
        {0.334f, 0}, {0.666f, 0}, // top
        {1, 0.333f}, {0.667f, 0.333f}, // back
        {0, 0.334f}, {0, 0.666f}, {0.333f, 0.666f}, {0.333f, 0.334f}, // bottom
        {0.334f, 0.334f}, {0.334f, 0.666f}, {0.666f, 0.666f}, {0.666f, 0.334f}, // left
        {0.667f, 0.334f}, {0.667f, 0.666f}, {1, 0.666f}, {1, 0.334f} // right
    };

    public void Awake()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh.uv = createUVMap(bounds);
    }

    private Vector2[] createUVMap(float[,] bounds) {
        var len = bounds.Length / 2;
        Vector2[] result = new Vector2[len];
        for (int i = 0; i < len; i++)
            result[i] = new Vector2(bounds[i, 0], bounds[i, 1]);
        return result;
    }
}
