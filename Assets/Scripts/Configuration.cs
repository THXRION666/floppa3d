using UnityEngine;

using System;

public class Configuration : MonoBehaviour
{
    private Transform floppa;
    private Rotator rotator;

    private Vector2 menuSize;
    private Vector2 size;
    private float indent;

    private Vector2 cursor;
    private Vector2 tempSize;

    private void Start()
    {
        floppa = GameObject.Find("Floppa").transform;
        rotator = floppa.GetComponent<Rotator>();
        
        menuSize = new Vector2(Screen.width / 4, Screen.height / 2);
        size = new Vector2(menuSize.x, menuSize.y * 0.1f);
        indent = size.y / 2;

        reset();
    }

    private void Slider(ref float value) 
    {
        var rect = new Rect(cursor, new Vector2(tempSize.x, tempSize.y / 10));
        value = GUI.HorizontalSlider(rect, value, -50, 50);
        cursor.y += size.y + indent;
    }

    private void SelectRotation(ref Vector3 rotation)
    {
        Slider(ref rotation.x);
        Slider(ref rotation.y);
        Slider(ref rotation.z);
    }

    private void OnGUI()
    {
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = Convert.ToInt32(size.y) - 5;

        GUI.Box(new Rect(cursor, menuSize), "Floppa rotation");
        
        cursor.x += indent;
        cursor.y += size.y + indent;
        tempSize.x -= indent * 2;

        SelectRotation(ref rotator.velocity);

        if (GUI.Button(new Rect(cursor, tempSize), "Freeze")) 
            rotator.velocity *= 0;

        cursor.y += indent * 3;

        if (GUI.Button(new Rect(cursor, tempSize), "Reset")) 
            floppa.eulerAngles = Vector3.zero;

        reset();
    }

    private void reset() {
        float x = (Screen.width / 2 - menuSize.x) / 2;
        float y = (Screen.height - menuSize.y) / 2;
        cursor = new Vector2(x, y);

        tempSize = size;
    }
}
